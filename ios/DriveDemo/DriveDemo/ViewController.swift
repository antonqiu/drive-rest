//
//  ViewController.swift
//  DriveDemo
//
//  Created by Antonio Qiu on 1/19/16.
//  Copyright © 2016 Anton Qiu. All rights reserved.
//

import UIKit

class ViewController: UIViewController, GIDSignInUIDelegate{
  
  
  @IBOutlet weak var signInButton: GIDSignInButton!
  @IBOutlet weak var logout: UIButton!
  @IBOutlet weak var disconnect: UIButton!
  @IBOutlet weak var status: UITextView!
  

  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    
    GIDSignIn.sharedInstance().uiDelegate = self
    
    // Uncomment to automatically sign in the user.
    GIDSignIn.sharedInstance().signInSilently()
    
    // TODO(developer) Configure the sign-in button look/feel
    // ...
    GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/drive.readonly")
    NSNotificationCenter.defaultCenter().addObserver(self,
      selector: "receiveToggleAuthUINotification:",
      name: "ToggleAuthUINotification",
      object: nil)
    toggleAuthUI()
  }
  @IBAction func didTapSignOut(sender: AnyObject) {
    GIDSignIn.sharedInstance().signOut()
    
    status.text = "Signed out."
    toggleAuthUI()
  }
  
  @IBAction func didTapDisconnect(sender: AnyObject) {
    GIDSignIn.sharedInstance().disconnect()
    status.text = "Disconnecting."
  }
  
  func toggleAuthUI() {
    if (GIDSignIn.sharedInstance().hasAuthInKeychain()){
      // Signed in
      signInButton.hidden = true
      logout.hidden = false
      disconnect.hidden = false
    } else {
      signInButton.hidden = false
      logout.hidden = true
      disconnect.hidden = true
      status.text = "Google Sign in\niOS Demo"
    }
  }
  
  deinit {
    NSNotificationCenter.defaultCenter().removeObserver(self,
      name: "ToggleAuthUINotification",
      object: nil)
  }
  
  @objc func receiveToggleAuthUINotification(notification: NSNotification) {
    if (notification.name == "ToggleAuthUINotification") {
      self.toggleAuthUI()
      if notification.userInfo != nil {
        let userInfo:Dictionary<String,String!> =
        notification.userInfo as! Dictionary<String,String!>
        self.status.text = userInfo["statusText"]
      }
    }
  }


  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
}

