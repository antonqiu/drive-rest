//
//  ViewController.swift
//  DriveOAuthDemo
//
//  Created by Antonio Qiu on 2/8/16.
//  Copyright © 2016 Anton Qiu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  
  private let kKeychainItemName = "Drive API"
  private let kClientID = "907555596285-p3hpcrmpgm8meiuvl290mpctisq9c19f.apps.googleusercontent.com"
  
  private let scopes = [kGTLAuthScopeDriveMetadataReadonly]
  
  private let service = GTLServiceDrive()
  let output = UITextView()
  
  // When the view loads, create necessary subviews
  // and initialize the Drive API service
  override func viewDidLoad() {
    super.viewDidLoad()
    
    output.frame = view.bounds
    output.editable = false
    output.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
    output.autoresizingMask = [UIViewAutoresizing.FlexibleHeight,
      UIViewAutoresizing.FlexibleWidth]
    
    view.addSubview(output);
    
    if let auth = GTMOAuth2ViewControllerTouch.authForGoogleFromKeychainForName(
      kKeychainItemName,
      clientID: kClientID,
      clientSecret: nil) {
        service.authorizer = auth
    }
    
  }
  
  // When the view appears, ensure that the Drive API service is authorized
  // and perform API calls
  override func viewDidAppear(animated: Bool) {
    if let authorizer = service.authorizer,
      canAuth = authorizer.canAuthorize where canAuth {
        fetchFiles()
    } else {
      presentViewController(
        createAuthController(),
        animated: true,
        completion: nil
      )
    }
  }
  
  // Construct a query to get names and IDs of 10 files using the Google Drive API
  func fetchFiles() {
    output.text = "Getting files..."
    let query = GTLQueryDrive.queryForFilesList()
    query.pageSize = 10
    query.fields = "nextPageToken, files(id, name)"
    service.executeQuery(
      query,
      delegate: self,
      didFinishSelector: "displayResultWithTicket:finishedWithObject:error:"
    )
  }
  
  // Parse results and display
  func displayResultWithTicket(ticket : GTLServiceTicket,
    finishedWithObject response : GTLDriveFileList,
    error : NSError?) {
      
      if let error = error {
        showAlert("Error", message: error.localizedDescription)
        return
      }
      
      var filesString = ""
      
      if let files = response.files where !files.isEmpty {
        filesString += "Files:\n"
        for file in files as! [GTLDriveFile] {
          filesString += "\(file.name) (\(file.identifier))\n"
        }
      } else {
        filesString = "No files found."
      }
      
      output.text = filesString
  }
  
  
  // Creates the auth controller for authorizing access to Drive API
  private func createAuthController() -> GTMOAuth2ViewControllerTouch {
    let scopeString = scopes.joinWithSeparator(" ");
    return GTMOAuth2ViewControllerTouch(
      scope: scopeString,
      clientID: kClientID,
      clientSecret: nil,
      keychainItemName: kKeychainItemName,
      delegate: self,
      finishedSelector: "viewController:finishedWithAuth:error:"
    )
  }
  
  // Handle completion of the authorization process, and update the Drive API
  // with the new credentials.
  func viewController(vc : UIViewController,
    finishedWithAuth authResult : GTMOAuth2Authentication, error : NSError?) {
      
      if let error = error {
        service.authorizer = nil
        showAlert("Authentication Error", message: error.localizedDescription)
        return
      }
      
      service.authorizer = authResult
      dismissViewControllerAnimated(true, completion: nil)
  }
  
  // Helper for showing an alert
  func showAlert(title : String, message: String) {
    let alert1 = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
//    let alert = UIAlertView(
//      title: title,
//      message: message,
//      delegate: nil,
//      cancelButtonTitle: "OK"
//    )
//    alert.show()
    let alert1_action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (UIAlertAction) -> Void in }
    alert1.addAction(alert1_action)
    presentViewController(alert1, animated: true, completion: nil)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
}

