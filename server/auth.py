from __future__ import print_function

import io
import logging
# import json

from bottle import Bottle, redirect, request
import httplib2
from apiclient import discovery, errors, http
from oauth2client import client
from excel import MSExcel

__version__ = '1.0.0'

app = Bottle()
logger = logging.getLogger('__auth__').setLevel(logging.DEBUG)

g = {
    'scopes': None,
    'credentials': None,
    'service': None,
    'from_page': '/'
}

g['scopes'] = dict(
    full = "https://www.googleapis.com/auth/drive",
    file_r = "https://www.googleapis.com/auth/drive.readonly",
    oneFile_rw = "https://www.googleapis.com/auth/drive.file",
    meta_rw = "https://www.googleapis.com/auth/drive.metadata",
    meta_r = "https://www.googleapis.com/auth/drive.metadata.readonly"
)

SCOPE = g['scopes'].get("file_r", "https://www.googleapis.com/auth/drive.readonly")
SECRET_NAME = 'client_secret_server.json'
# app's auth endpoint handles response from OAUTH server
REDIRECT_URI = 'http://localhost:8080/auth'


@app.route('/auth')
def auth():
    # OAuth2 Client Initialization
    flow = client.flow_from_clientsecrets(SECRET_NAME, SCOPE, REDIRECT_URI)

    # AUTH
    if 'code' not in request.query:
        # get auth uri from the client
        auth_uri = flow.step1_get_authorize_url()
        return redirect(auth_uri)
    # RESPONSE
    else:
        # get authorization code -> tokens
        auth_code = request.query.code
        g['credentials'] = flow.step2_exchange(code=auth_code)
        # HTTP object
        http_auth = g['credentials'].authorize(httplib2.Http())
        # API
        g['service'] = discovery.build('drive', 'v3', http=http_auth)
        return redirect(g['from_page'])


@app.route('/')
def index():
    if not credential_valid():
        g['from_page'] = '/'
        return redirect(app.get_url('/auth'))
    else:
        files = g['service'].files().list(
                pageSize=10, fields="nextPageToken, files(id, name)").execute()
        result = ''
        for item in files.get('files', []):
           result += 'filename:{} | fileid: {}<br>'.format(item['name'], item['id'])
        # return result
        return result


@app.route('/file/<fileid>/export/<mime>')
def export(fileid, mime):
    if not credential_valid():
        g['from_page'] = '/file/{}/export/{}'.format(fileid, mime)
        return redirect(app.get_url('/auth'))
    else:
        MIMEType = mimeParser(mime)
        # supported MIME type from google spreadsheet:
        # application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
        # application/x-vnd.oasis.opendocument.spreadsheet
        # application/pdf
        # text/csv (internal error, 500)
        try:
            req = g['service'].files().export_media(fileId=fileid, mimeType=MIMEType)
            fh = io.BytesIO()
            fo = open('Download.{}'.format(mime), 'wb')
            downloader = http.MediaIoBaseDownload(fh, req)
            done = False
            while done is False:
                status, done = downloader.next_chunk()
                print('Download {}%'.format(status.progress() * 100))
        except errors.HttpError as e:
            print(e)
            return 'Error occurred: {}'.format(e.content)
        else:
            buffer = fh.getvalue()
            fo.write(buffer)
            # buffer = fh.read(1000)
            # while len(buffer):
            #     fo.write(buffer)
            #     print('.', end='')
            #     buffer = fh.read1(1000)
            fo.close()
            if mime == 'xls' or mime == 'xlsx':
                wb = MSExcel(fo.name)
                result = wb.getByColumn('A', 1, 10)
            return str(result)+'<br>downloaded: ./{} | size: {} bytes'.format(fo.name, len(buffer))

@app.route('/file/<fileid>/get')
def get(fileid):
    if not credential_valid():
        g['from_page'] = '/file/{}/get'.format(fileid)
        return redirect(app.get_url('/auth'))
    else:
        try:
            req = g['service'].files().get(fileId=fileid)
            fh = io.BytesIO()
            downloader = http.MediaIoBaseDownload(fh, req)
            done = False
            while done is False:
                status, done = downloader.next_chunk()
                print('Download {}%'.format(status.progress() * 100))
        except errors.HttpError as e:
            print(e)
            return 'Error occurred: {}'.format(e.content)
        else:
            fo = open(req.execute()['name'], 'wb')
            buffer = fh.getvalue()
            fo.write(buffer)
            # buffer = fh.read(1000)
            # while len(buffer):
            #     fo.write(buffer)
            #     print('.', end='')
            #     buffer = fh.read1(1000)
            fo.close()
            return 'downloaded: ./{} | size: {} bytes'.format(fo.name, len(buffer))


def credential_valid():
    if not g['credentials'] or not g['service']:
        return False
    elif g['credentials'].access_token_expired:
        return False
    else:
        return True

def mimeParser(mime):
    specialMIME = dict(
        xlsx='vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        xls='vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        ods='application/x-vnd.oasis.opendocument.spreadsheet',
        docx='vnd.openxmlformats-officedocument.wordprocessingml.document',
        doc='vnd.openxmlformats-officedocument.wordprocessingml.document',
        odt='vnd.oasis.opendocument.text',
        ppt='vnd.openxmlformats-officedocument.presentationml.presentation',
        pptx='vnd.openxmlformats-officedocument.presentationml.presentation'
    )
    mime = specialMIME.get(mime, mime)
    choices = dict(
        html='text',
        plain='text',
        rtf='application',
        pdf='application',
        csv='text',
        jpeg='image',
        png='image'
    )
    MIMEType = '{}/{}'.format(choices.get(mime, 'application'), mime)
    return MIMEType


if __name__ == '__main__':
    print('To start, visit the index page', 'http://localhost:8080' + app.get_url('/'))
    print('It may not work on OS X. (Unless do a global replace localhost with 127.0.0.1)')
    app.run(host='localhost', port=8080)
